package pl.androidwpraktyce.szkoleniew3_pliki;

import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class MainActivity extends ActionBarActivity {

	public static final String FILE_NAME = "szkolenieW3.txt";

	@InjectView(R.id.pole_edycji)	// Zamiast findViewById
	protected EditText mPoleTekstowe;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Wstrzykiwanie w pola obiektów widoków
		// oraz przypięcie onClickListenerów
		// Mechanizm wstrzykiwania widoków - injection view
		ButterKnife.inject(this);	// To musi być !!!
	}

	@OnClick(R.id.btn_save_internal)
	public void saveInternal() {
//		Toast.makeText(this, mPoleTekstowe.getText().toString(), Toast.LENGTH_SHORT).show();
		// Scieżka do katalogu aplikacji
		// Nie musimy sprawdzać czy jest do zapisu/odczytu
		File mFilesDir = getFilesDir();	// wskazuje na folder !
		File mPlikTekstowy = new File(mFilesDir, FILE_NAME);

		writeToFile(mPlikTekstowy);
	}

	@OnClick(R.id.btn_save_external)
	public void saveExternal() {
		if (isExternalStorageWritable()) {
			File mExternalDir = Environment
					.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
			File mAppDir = new File(mExternalDir, "szkolenieW3");

			if (!mAppDir.exists()) {
				// Założenie folderów których brakuje
				mAppDir.mkdirs();
			}

			File mTargetFile = new File(mAppDir, FILE_NAME);

			Log.d("FileSave", mTargetFile.getAbsolutePath());
			writeToFile(mTargetFile);
		} else {
			Toast.makeText(this, "External Storage niedostępne !", Toast.LENGTH_SHORT).show();
		}
	}

	@OnClick(R.id.btn_read_internal)
	public void readInternal() {
		File mFilesDir = getFilesDir();	// wskazuje na folder !
		File mPlikTekstowy = new File(mFilesDir, FILE_NAME);

		readFromFile(mPlikTekstowy);
	}



	@OnClick(R.id.btn_read_external)
	public void readExternal() {
		if (isExternalStorageWritable()) {
			File mExternalDir = Environment
					.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
			File mAppDir = new File(mExternalDir, "szkolenieW3");

			if (!mAppDir.exists()) {
				// Założenie folderów których brakuje
				mAppDir.mkdirs();
			}

			File mTargetFile = new File(mAppDir, FILE_NAME);

			Log.d("FileRead", mTargetFile.getAbsolutePath());
			readFromFile(mTargetFile);
		} else {
			Toast.makeText(this, "External Storage niedostępne !", Toast.LENGTH_SHORT).show();
		}
	}

	// Zwraca informacje czy można zapisywać do External Storage
	private boolean isExternalStorageWritable() {
		String mExternalState = Environment.getExternalStorageState();
		return mExternalState.equals(Environment.MEDIA_MOUNTED);
	}

	private void writeToFile(File mPlikTekstowy) {
		try {
			/// Tworzymy strumień wyjścia do pliku
			FileOutputStream mFileOutput = new FileOutputStream(mPlikTekstowy);
			IOUtils.write(mPoleTekstowe.getText(), mFileOutput);
			mFileOutput.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void readFromFile(File mPlikTekstowy) {
		if (mPlikTekstowy.exists()) {
			// Odczyt z pliku
			try {
				// Otwarcie strumienia wejściowego do pliku
				FileInputStream mFileInput = new FileInputStream(mPlikTekstowy);
				// Odczytanie strumienia z pliku do zmiennej typu String
				String mTekst = IOUtils.toString(mFileInput);

				// Wpisanie odczytaniego tekstu do pola tekstowego
				mPoleTekstowe.setText(mTekst);

				mFileInput.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			//Informacja o braku pliku !
			Toast.makeText(this, "Nie znaleziono pliku !", Toast.LENGTH_SHORT).show();
		}
	}




	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
